class Comment < ActiveRecord::Base
  belongs_to :post
  validates_presence_of :body
  validates :post_id, :presence => true
  
end